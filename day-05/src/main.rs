use std::cmp::max;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

fn get_seat_id(s: &str) -> u32 {
    let (mut xmin, mut xmax) = (0, 8);
    let (mut ymin, mut ymax) = (0, 128);

    for c in s.chars() {
        match c {
            'F' => {
                ymax -= (ymax - ymin) / 2;
            }
            'B' => {
                ymin += (ymax - ymin) / 2;
            }
            'L' => {
                xmax -= (xmax - xmin) / 2;
            }
            'R' => {
                xmin += (xmax - xmin) / 2;
            }
            _ => panic!(),
        }
        // println!("{} => x=[{};{}[ y=[{};{}[", c, xmin, xmax, ymin, ymax);
    }

    xmin + ymin * 8
}

fn main() {
    assert_eq!(get_seat_id("BFFFBBFRRR"), 567);
    assert_eq!(get_seat_id("FFFBBBFRRR"), 119);
    assert_eq!(get_seat_id("BBFFBBFRLL"), 820);

    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines();
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2 = 0;

    let mut seats = [false; 128 * 8];

    for l in lines {
        if let Ok(l) = l {
            let id = get_seat_id(&l);

            res_part1 = max(id, res_part1);
            seats[id as usize] = true;
        }
    }

    for id in 8..127 * 8 {
        if !seats[id] && seats[id - 1] && seats[id + 1] {
            res_part2 = id;
            break;
        }
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert_eq!(res_part1, 801);
    assert!(res_part2 == 597);
}
