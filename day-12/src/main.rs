use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug)]
struct Ship {
    position: (i64, i64),
    direction: char,
}

fn dir_to_u8(dir: char) -> u8 {
    match dir {
        'N' => 0,
        'W' => 1,
        'S' => 2,
        'E' => 3,
        _ => panic!(dir),
    }
}

fn u8_to_dir(dir: u8) -> char {
    ['N', 'W', 'S', 'E'][dir as usize]
}

impl Ship {
    fn rotate(&mut self, dir: char, amount: i64) {
        assert!(amount >= 0);
        assert!(amount % 90 == 0);
        let rot_steps = ((amount / 90) % 4) as u8;

        let new_dir = match dir {
            'L' => (dir_to_u8(self.direction) + rot_steps) % 4,
            'R' => (dir_to_u8(self.direction) + (4 - rot_steps)) % 4,
            _ => panic!(),
        };
        self.direction = u8_to_dir(new_dir);
    }
    fn move_dir(&mut self, dir: char, dist: i64) {
        match dir {
            'N' => self.position.1 += dist,
            'W' => self.position.0 -= dist,
            'S' => self.position.1 -= dist,
            'E' => self.position.0 += dist,
            _ => panic!(),
        };
    }
    fn forward(&mut self, dist: i64) {
        self.move_dir(self.direction, dist);
    }

    fn rotate_wp(&mut self, dir: char, amount: i64) {
        let mut rot_steps = ((amount / 90) % 4) as u8;
        if dir == 'R' {
            rot_steps = 4 - rot_steps;
        }

        // (x, y)
        // L (-y x)
        // L (-x -y)
        // L (y -x)
        self.position = match rot_steps {
            0 => self.position,
            1 => (-self.position.1, self.position.0),
            2 => (-self.position.0, -self.position.1),
            3 => (self.position.1, -self.position.0),
            _ => panic!(),
        };
    }
    fn forward_wp(&self, from: (i64, i64), dist: i64) -> (i64, i64) {
        (
            from.0 + self.position.0 * dist,
            from.1 + self.position.1 * dist,
        )
    }
}

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, i64>::new();

    let mut actions = vec![];
    for line in lines {
        let mut c = line.chars();
        let op = c.next().unwrap();
        let value = c.as_str().parse::<i64>().unwrap();

        actions.push((op, value));
    }

    // P1
    let mut ship = Ship {
        position: (0, 0),
        direction: 'E',
    };
    for act in &actions {
        match act.0 {
            'F' => ship.forward(act.1),
            'L' | 'R' => ship.rotate(act.0, act.1),
            'N' | 'S' | 'E' | 'W' => ship.move_dir(act.0, act.1),
            _ => panic!(),
        };
    }
    aoc.set_p1(ship.position.0.abs() + ship.position.1.abs());

    // P2
    let mut ship = Ship {
        position: (10, 1),
        direction: 'E',
    };
    let mut ship_real_pos = (0, 0);
    for act in &actions {
        match act.0 {
            'F' => {
                ship_real_pos = ship.forward_wp(ship_real_pos, act.1);
            }
            'L' | 'R' => ship.rotate_wp(act.0, act.1),
            'N' | 'S' | 'E' | 'W' => ship.move_dir(act.0, act.1),
            _ => panic!(),
        };
    }
    aoc.set_p2(ship_real_pos.0.abs() + ship_real_pos.1.abs());

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 757);
            assert_eq!(aoc.p2(), 51249);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 25);
            assert_eq!(aoc.p2(), 286);
        }
        _ => panic!(),
    }
}
