use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines();
    let start = SystemTime::now();

    let mut values = vec![];
    let mut res_a = None;
    let mut res_b = None;

    for l in lines {
        if let Ok(l) = l {
            // println!("{:?}", l);
            values.push(l.parse::<i64>().unwrap());
        }
    }
    values.sort();

    let mut min = 0;
    let mut max = values.len() - 1;
    while min != max {
        let res = values[min] + values[max];
        if res == 2020 {
            res_a = Some(values[min] * values[max]);
            println!(
                "{} + {} = 2020 => a * b = {}",
                values[min],
                values[max],
                res_a.unwrap()
            );
            break;
        } else if res < 2020 {
            min += 1;
        } else {
            max -= 1;
        }
    }

    // YOL-O(n³)
    for (ia, a) in values.iter().enumerate() {
        for (ib, b) in values[ia + 1..values.len()].iter().enumerate() {
            for c in &values[ib + 1..values.len()] {
                if a + b + c == 2020 {
                    println!("{} + {} + {} = 2020 => a * b * c = {}", a, b, c, a * b * c);
                    res_b = Some(a * b * c);
                    break;
                }
            }
            if let Some(_) = res_b {
                break;
            }
        }
        if let Some(_) = res_b {
            break;
        }
    }

    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert!(res_a == Some(1003971));
    assert!(res_b == Some(84035952));
}
