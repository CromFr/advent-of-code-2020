use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines();
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2: i64 = 1;

    let mut map = vec![];
    for l in lines {
        if let Ok(l) = l {
            // println!("{:?}", l);
            map.push(l.chars().collect::<Vec<char>>());
        }
    }
    let map = map;

    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    for (i, slope) in slopes.iter().enumerate() {
        let (mut x, mut y) = (0, 0);
        let width = map[0].len();
        let height = map.len();

        let mut trees = 0;

        while y < height {
            if map[y][x] == '#' {
                trees += 1;
            }

            x = (x + slope.0) % width;
            y += slope.1;
        }

        if i == 1 {
            res_part1 = trees;
        }
        res_part2 *= trees;
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert!(res_part1 == 240);
    assert!(res_part2 == 2832009600);
}
