use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<u64, u64>::new();

    let mut start_ts = 0;
    let mut bus_list = vec![];

    for (i, line) in lines.enumerate() {
        match i {
            0 => start_ts = line.parse::<u64>().unwrap(),
            1 => {
                bus_list = line
                    .split(',')
                    .enumerate()
                    .filter(|a| a.1 != "x")
                    .map(|a| (a.0 as u64, a.1.parse::<u64>().unwrap()))
                    .collect()
            }
            _ => panic!(),
        }
    }

    let mut earliest_bus: Option<(u64, u64)> = None;
    for bus in &bus_list {
        let wait_time = bus.1 - ((start_ts - 1) % bus.1 + 1);
        // println!("{:?} wait for {}", bus, wait_time);

        if earliest_bus.is_none() || wait_time < earliest_bus.unwrap().1 {
            earliest_bus = Some((bus.1, wait_time));
        }
    }
    aoc.set_p1(earliest_bus.unwrap().0 * earliest_bus.unwrap().1);

    // // P2
    // // Brute force approach
    // bus_list.sort_by_key(|a| a.1);
    // bus_list.reverse();
    // let max_bus = bus_list[0];

    // let mut curr_mult = start_ts / max_bus.1 + 1;
    // loop {
    //     let ts = curr_mult * max_bus.1 - max_bus.0;
    //     if curr_mult % 10000000 == 0 {
    //         println!("iter {} => ts={}", curr_mult, ts);
    //     }

    //     let mut all_valid = true;
    //     for bus in bus_list[1..].iter() {
    //         if bus.1 - ((ts - 1) % bus.1 + 1) != bus.0 {
    //             all_valid = false;
    //             break;
    //         }
    //     }

    //     if all_valid {
    //         aoc.set_p2(ts);
    //         break;
    //     }
    //     curr_mult += 1;
    // }

    let rem_mod_list: Vec<(u64, u64)> = bus_list.iter().map(|a| (a.1 - a.0 % a.1, a.1)).collect();
    // Chinese remainder theorem
    // Based on https://www.youtube.com/watch?v=BYSmWLLBKC0
    //
    // 7,13,x,x,59,x,31,19
    // ts === 0 (mod 7)
    // ts === 1 (mod 13)
    // ts === 4 (mod 59)
    // ts === 6 (mod 31)
    // ts === 7 (mod 19)

    // N = 7 * 13 * 59 * 31 * 19 = 3162341
    // N0=     13 * 59 * 31 * 19 = 451763
    // N1= 7      * 59 * 31 * 19 = 243257
    // N2= 7 * 13      * 31 * 19 = 53599
    // N3= 7 * 13 * 59      * 19 = 102011
    // N4= 7 * 13 * 59 * 31      = 166439
    let n = rem_mod_list.iter().fold(1, |acc, rem_mod| acc * rem_mod.1);
    let n_list: Vec<u64> = rem_mod_list.iter().map(|rem_mod| n / rem_mod.1).collect();
    println!("n_list={:?}", n_list);

    // N0 X0 === 1 (mod 7)
    // => (N0 % 7) X0 === 1 (mod 7)
    // => 4 X0 === 1 (mod 7)
    // => X0 = 2

    // N1 X1 === 1 (mod 13)
    // => (N1 % 13) X1 === 1 (mod 13)
    // => 1 X1 === 1 (mod 13)

    // N2 X2 === 1 (mod 59)
    // => (N2 % 59) X2 === 1 (mod 59)
    // => 27 X2 === 1 (mod 59)
    // => X2 = 35
    let x_list: Vec<u64> = rem_mod_list
        .iter()
        .enumerate()
        .map(|(i, rem_mod)| {
            // n_list[i] * x === 1  (mod rem_mod.1)
            let a = n_list[i] % rem_mod.1;

            // Brute force modulo solution
            for j in 1..rem_mod.1 {
                if (a * j) % rem_mod.1 == 1 {
                    return j;
                }
            }
            panic!("Nothing found");
        })
        .collect();
    println!("x_list={:?}", x_list);

    let x: u64 = rem_mod_list
        .iter()
        .enumerate()
        .map(|(i, rem_mod)| x_list[i] * n_list[i] * rem_mod.0)
        .sum();
    println!("x={:?}", x);

    aoc.set_p2(x % n);

    // Checking everything is correct
    for bus in &bus_list {
        let wait_time = bus.1 - ((aoc.p2() - 1) % bus.1 + 1);
        assert_eq!(
            wait_time,
            bus.0 % bus.1,
            "bus={:?} mod={} => {}",
            bus,
            aoc.p2() % bus.1,
            wait_time
        );
    }

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 136);
            assert_eq!(aoc.p2(), 305068317272992);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 295);
            assert_eq!(aoc.p2(), 1068781);
        }
        _ => panic!(),
    }
}
