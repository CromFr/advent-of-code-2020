use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

#[derive(Debug, Clone)]
struct Context {
    pc: usize,
    acc: i32,
    executed: Vec<bool>,
}

impl Context {
    fn new(program_size: usize) -> Context {
        Context {
            pc: 0,
            acc: 0,
            executed: vec![false; program_size],
        }
    }
    fn execute(&mut self, instruction: &(String, i32)) {
        self.executed[self.pc] = true;
        match instruction.0.as_str() {
            "acc" => self.acc += instruction.1,
            "jmp" => {
                self.pc = (self.pc as i32 + instruction.1) as usize;
                return;
            }
            "nop" => (),
            _ => panic!(),
        }

        self.pc += 1;
    }
    fn execute_program(&mut self, program: &Vec<(String, i32)>) -> Result<i32, i32> {
        loop {
            if self.pc == program.len() {
                return Ok(self.acc);
            } else if self.pc > program.len() {
                panic!();
            }

            if self.executed[self.pc] {
                // Loop detected
                return Err(self.acc);
            }
            self.execute(&program[self.pc]);
        }
    }
}

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines().filter_map(Result::ok);
    let start = SystemTime::now();

    #[allow(unused_assignments)]
    let mut res_part1 = 0;
    #[allow(unused_assignments)]
    let mut res_part2 = 0;

    let mut program: Vec<(String, i32)> = vec![];
    for line in lines {
        let s: Vec<&str> = line.split(' ').collect();
        assert!(s.len() == 2);
        program.push((s[0].to_owned(), s[1].parse::<i32>().unwrap()));
    }

    let mut ctx = Context::new(program.len());
    match ctx.execute_program(&program) {
        Err(res) => res_part1 = res,
        _ => panic!(),
    }

    // // Part 2: stupid brute force ~55ms
    // for i in 0..program.len() {
    //     if program[i].0 == "acc" {
    //         continue;
    //     }

    //     let mut program = program.clone();
    //     match program[i].0.as_str() {
    //         "jmp" => program[i].0 = "nop".to_string(),
    //         "nop" => program[i].0 = "jmp".to_string(),
    //         _ => panic!(),
    //     }

    //     ctx = Context::new(program.len());
    //     loop {
    //         if ctx.pc == program.len() {
    //             res_part2 = ctx.acc;
    //             break;
    //         }

    //         if ctx.executed[ctx.pc] {
    //             // Loop detected
    //             break;
    //         }
    //         ctx.execute(&program[ctx.pc]);
    //     }
    // }

    // Part 2: smarter brute force -> ~4.4ms
    ctx = Context::new(program.len());
    loop {
        if program[ctx.pc].0 != "acc" {
            let mut new_ctx = ctx.clone();

            let modified_instruction = match program[ctx.pc].0.as_str() {
                "jmp" => ("nop".to_string(), program[ctx.pc].1),
                "nop" => ("jmp".to_string(), program[ctx.pc].1),
                _ => panic!(),
            };

            new_ctx.execute(&modified_instruction);

            if let Ok(res) = new_ctx.execute_program(&program) {
                res_part2 = res;
                println!(
                    "Executed {} / {} instructions",
                    new_ctx.executed.iter().fold(0, |acc, x| acc + *x as usize),
                    program.len()
                );
                break;
            }
        }

        ctx.execute(&program[ctx.pc]);
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert_eq!(res_part1, 1487);
    assert_eq!(res_part2, 1607);
}
