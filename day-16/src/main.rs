use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<_, u64>::new();

    enum State {
        RANGES,
        YOURTICK,
        NEARTICK,
    }
    let mut state = State::RANGES;

    let mut rules: Vec<(String, Vec<(u32, u32)>)> = vec![];
    let mut my_ticket: Vec<u32> = vec![];
    let mut near_tickets: Vec<Vec<u32>> = vec![];

    for line in lines {
        if line.len() == 0 {
            continue;
        }
        match line.as_str() {
            "your ticket:" => {
                state = State::YOURTICK;
                continue;
            }
            "nearby tickets:" => {
                state = State::NEARTICK;
                continue;
            }
            _ => (),
        }

        match state {
            State::RANGES => {
                let sep = line.find(": ").unwrap();
                let field_name = &line[0..sep];
                let value = &line[sep + 2..];

                let line_ranges: Vec<_> = value
                    .split(" or ")
                    .map(|a| {
                        let s = a
                            .split('-')
                            .map(|a| a.parse::<u32>().unwrap())
                            .collect::<Vec<_>>();
                        (s[0], s[1])
                    })
                    .collect();

                rules.push((field_name.to_owned(), line_ranges));
            }
            State::YOURTICK | State::NEARTICK => {
                let tick = line.split(',').map(|a| a.parse::<u32>().unwrap()).collect();
                match state {
                    State::YOURTICK => {
                        my_ticket = tick;
                    }
                    State::NEARTICK => {
                        near_tickets.push(tick);
                    }
                    _ => panic!(),
                }
            }
        }
    }

    fn is_in_field_ranges(value: u32, field_ranges: &Vec<(u32, u32)>) -> bool {
        field_ranges.iter().any(|r| r.0 <= value && value <= r.1)
    }

    // P1
    let mut invalid_sum = 0;
    let mut valid_tickets = vec![];
    for ticket in &near_tickets {
        let mut valid = true;
        for f in ticket {
            if !rules.iter().any(|ranges| is_in_field_ranges(*f, &ranges.1)) {
                invalid_sum += f;
                valid = false;
            }
        }
        if valid {
            valid_tickets.push(ticket);
        }
    }
    aoc.set_p1(invalid_sum);

    // P2

    // Calculate a bitmask for each field, for which rule is valid for the field values
    let mask: u32 = (1 << rules.len()) - 1;
    let mut field_possible_rules = vec![mask; rules.len()]; // field_possible_rules[field_index] -> possible_rules
    for ticket in &valid_tickets {
        for (field_index, field_value) in ticket.iter().enumerate() {
            let mut and_mask = mask;
            for (rule_index, rule) in rules.iter().enumerate() {
                if !is_in_field_ranges(*field_value, &rule.1) {
                    and_mask &= !(1u32 << rule_index);
                }
            }
            assert!(and_mask & field_possible_rules[field_index] > 0);
            if and_mask != mask {
                field_possible_rules[field_index] &= and_mask;
                assert!(field_possible_rules[field_index].count_ones() >= 1);
            }
        }
    }

    // println!("field_index: possible_rules");
    // for (i, f) in field_possible_rules.iter().enumerate() {
    //     println!("{:02}: {:020b}", i, *f);
    // }

    let mut field_to_rule = vec![usize::MAX; field_possible_rules.len()];
    let mut current_mask = mask;

    // // ==> O(n log(n)) solution, by exploiting the field generation algorithm
    // // In the input provided, each field can match a incrementing number of rules
    // let mut solutions: Vec<_> = field_possible_rules
    //     .iter()
    //     .map(|a| a.count_ones())
    //     .enumerate()
    //     .collect();
    // solutions.sort_by_key(|a| a.1);
    // for (field_index, _) in solutions {
    //     let remaining_candidate = field_possible_rules[field_index] & current_mask;
    //     field_to_rule[field_index] = remaining_candidate.trailing_zeros() as usize;
    //     current_mask &= remaining_candidate ^ mask;
    // }

    // ==> O(n²) solution, but will always work
    // Also it turns out the overhead of the O(n log(n)) solution makes it faster for the given input
    for _ in 0..field_possible_rules.len() {
        for (field_index, possible_rules) in field_possible_rules.iter().enumerate() {
            let remaining_candidate = possible_rules & current_mask;
            if remaining_candidate.count_ones() == 1 {
                assert!(field_to_rule[field_index] == usize::MAX);
                field_to_rule[field_index] = remaining_candidate.trailing_zeros() as usize;
                current_mask &= remaining_candidate ^ mask;
                break;
            }
        }
    }

    // println!("field_to_rule: {:?}", field_to_rule);

    aoc.set_p2(
        field_to_rule
            .iter()
            .enumerate()
            .map(|(field_index, rule_index)| {
                if rules[*rule_index].0.starts_with("departure") {
                    my_ticket[field_index]
                } else {
                    1
                }
            })
            .fold(1u64, |acc, a| acc * (a as u64)),
    );

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 22057);
            assert_eq!(aoc.p2(), 1093427331937);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 71);
            assert_eq!(aoc.p2(), 0);
        }
        _ => panic!(),
    }
}
