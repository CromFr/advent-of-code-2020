use aoc_lib::AOCResults;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

enum Rule {
    Char(char),
    Aggregate(Vec<Vec<usize>>),
}

fn make_regex_str(rules: &HashMap<usize, Rule>, index: usize) -> String {
    match rules.get(&index).unwrap() {
        Rule::Char(c) => c.to_string(),
        Rule::Aggregate(aggr) => {
            let mut rgx = String::new();
            let or_groups = aggr
                .iter()
                .map(|and_group| {
                    and_group
                        .iter()
                        .map(|r| make_regex_str(rules, *r))
                        .collect::<Vec<_>>()
                        .join("")
                })
                .collect::<Vec<String>>();

            if aggr.len() > 1 {
                rgx.push_str(&format!("(?:{})", or_groups.join("|")));
            } else {
                rgx.push_str(&or_groups[0]);
            }

            rgx
        }
    }
}

fn match_str(rules: &HashMap<usize, Rule>, data: &str, rules_to_explore: &mut Vec<usize>) -> bool {
    // println!("match_str '{:?}' with rules {:?}", data, rules_to_explore);
    if data.len() == 0 && rules_to_explore.len() == 0 {
        return true;
    } else if data.len() == 0 || rules_to_explore.len() == 0 {
        return false;
    }

    let last_rule = &rules[&rules_to_explore.pop().unwrap()];
    match last_rule {
        Rule::Char(c) => {
            if data.len() > 0 && data.chars().next().unwrap() == *c {
                match_str(rules, &data[1..], rules_to_explore)
            } else {
                false
            }
        }
        Rule::Aggregate(or_groups) => {
            for seq in or_groups {
                let mut rtm: Vec<_> = rules_to_explore
                    .iter()
                    .cloned()
                    .chain(seq.iter().cloned().rev())
                    .collect();
                if match_str(rules, data, &mut rtm) {
                    return true;
                }
            }
            false
        }
    }
}

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut rules = HashMap::<usize, Rule>::new();
    let mut data = vec![];

    let mut header = true;
    for line in lines {
        if line.len() == 0 {
            header = false;
            continue;
        }

        if header {
            let col = line.find(':').unwrap();
            let index = line[0..col].parse::<usize>().unwrap();
            let rule_str = &line[col + 2..];

            if rule_str.len() == 3
                && rule_str.chars().nth(0) == Some('"')
                && rule_str.chars().nth(2) == Some('"')
            {
                rules.insert(index, Rule::Char(rule_str.chars().nth(1).unwrap()));
            } else {
                rules.insert(
                    index,
                    Rule::Aggregate(
                        rule_str
                            .split('|')
                            .map(|a| {
                                a.split(' ')
                                    .filter(|a| a.len() > 0)
                                    .map(|a| a.parse::<usize>().unwrap())
                                    .collect::<Vec<_>>()
                            })
                            .collect::<Vec<_>>(),
                    ),
                );
            }
        } else {
            data.push(line);
        }
    }

    let mut aoc = AOCResults::<i64, i64>::new();

    let rgx_str = format!(r"^{}$", make_regex_str(&rules, 0));
    let rgx = Regex::new(&rgx_str).unwrap();

    aoc.set_p1(data.iter().map(|a| rgx.is_match(a) as i64).sum());

    // 8: 42 | 42 8
    // 11: 42 31 | 42 11 31
    rules.insert(8, Rule::Aggregate(vec![vec![42], vec![42, 8]]));
    rules.insert(11, Rule::Aggregate(vec![vec![42, 31], vec![42, 11, 31]]));

    aoc.set_p2(
        data.iter()
            .map(|a| match_str(&rules, a, &mut vec![0]) as i64)
            .sum(),
    );

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 190);
            assert_eq!(aoc.p2(), 311);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 2);
            // assert_eq!(aoc.p2(), 848);
        }
        _ => panic!(),
    }
}
