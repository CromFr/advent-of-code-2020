use aoc_lib::AOCResults;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, &str>::new();

    let foods: Vec<_> = lines
        .map(|line| {
            if let Some(contains_pos) = line.find(" (contains ") {
                (
                    line[..contains_pos]
                        .split(" ")
                        .map(str::to_owned)
                        .collect::<HashSet<_>>(),
                    line[contains_pos + 11..line.len() - 1]
                        .split(", ")
                        .map(str::to_owned)
                        .collect::<HashSet<_>>(),
                )
            } else {
                (
                    line.split(" ").map(str::to_owned).collect::<HashSet<_>>(),
                    HashSet::new(),
                )
            }
        })
        .collect();

    let mut ingredients: Vec<&str> = vec![];
    let mut ingredient_indices = HashMap::<&str, usize>::new();
    let mut allergens: Vec<&str> = vec![];
    let mut allergen_indices = HashMap::<&str, usize>::new();
    for food in &foods {
        for i in &food.0 {
            if !ingredient_indices.contains_key::<str>(i) {
                ingredient_indices.insert(i, ingredients.len());
                ingredients.push(i);
            }
        }
        for a in &food.1 {
            if !allergen_indices.contains_key::<str>(a) {
                allergen_indices.insert(a, allergens.len());
                allergens.push(a);
            }
        }
    }

    println!("foods={:?}", foods);
    println!("ingredients={:?}", ingredients);
    println!("allergens={:?}", allergens);

    // mxmxvkd kfcds sqjhc nhms (contains dairy, fish, ...?)
    // trh fvjkl sbzzf mxmxvkd (contains dairy, ...?)
    // sqjhc fvjkl (contains soy, ...?)
    // sqjhc mxmxvkd sbzzf (contains fish, ...?)
    //
    // Cannot contain allergens: kfcds, nhms, sbzzf, or trh
    //
    // dairy in [mxmxvkd kfcds sqjhc nhms, trh fvjkl sbzzf mxmxvkd]. mxmxvkd is the only common ingredient => mxmxvkd contains dairy
    // fish in [mxmxvkd kfcds sqjhc nhms, sqjhc mxmxvkd sbzzf]. mxmxvkd & sqjhc are on both => mxmxvkd is known => sqjhc is fish
    // soy in [sqjhc fvjkl]. sqjhc & fvjkl may be soy

    let mut ingr_for_allerg = vec![vec![]; allergens.len()];
    for food in &foods {
        for allerg in &food.1 {
            let allerg_index = *allergen_indices.get::<str>(allerg).unwrap();
            ingr_for_allerg[allerg_index].push(&food.0);
        }
    }

    println!("ingr_for_allerg={:?}", ingr_for_allerg);

    let mut ingr_to_allerg = HashMap::<String, String>::new();

    let mut known_set = HashSet::<String>::new();
    loop {
        let mut added = false;
        for (i, ingr_sets) in ingr_for_allerg.iter().enumerate() {
            let mut set: Option<HashSet<String>> = None;
            for ingr_set in ingr_sets.iter() {
                set = match set {
                    None => Some((*ingr_set).clone()),
                    Some(set) => Some(set.intersection(ingr_set).cloned().collect()),
                };
            }
            let set: HashSet<_> = set.unwrap().difference(&known_set).cloned().collect();
            if set.len() == 1 {
                println!(
                    "Allergen {} is inside {:?}",
                    allergens[i],
                    set.iter().next().unwrap().clone()
                );
                let ingredient = set.iter().next().unwrap();
                known_set.insert(ingredient.clone());
                ingr_to_allerg.insert(ingredient.clone(), allergens[i].to_string());
                added = true;
            }
        }
        if !added {
            break;
        }
    }

    aoc.set_p1(
        foods
            .iter()
            .map(|food| {
                food.0
                    .iter()
                    .map(|a| !ingr_to_allerg.contains_key(a) as i64)
                    .sum::<i64>()
            })
            .sum(),
    );

    let mut allergen_assoc = ingr_to_allerg.into_iter().collect::<Vec<_>>();
    allergen_assoc.sort_by_key(|a| a.1.clone());
    let p2_res = allergen_assoc
        .iter()
        .map(|a| a.0.clone())
        .collect::<Vec<String>>()
        .join(",");
    aoc.set_p2(&p2_res);

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 2786);
            assert_eq!(aoc.p2(), "prxmdlz,ncjv,knprxg,lxjtns,vzzz,clg,cxfz,qdfpq");
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 5);
            assert_eq!(aoc.p2(), "mxmxvkd,sqjhc,fvjkl");
        }
        _ => panic!(),
    }
}
