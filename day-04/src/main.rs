use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

extern crate lazy_static;
use lazy_static::lazy_static;

fn check_accu_p1(accu: &HashMap<String, String>) -> bool {
    accu.len() == 8 || (accu.len() == 7 && !accu.contains_key("cid"))
}

// we expect that required keys are present
fn check_accu_p2(accu: &HashMap<String, String>) -> bool {
    lazy_static! {
        // byr (Birth Year) - four digits; at least 1920 and at most 2002.
        // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        // hgt (Height) - a number followed by either cm or in:
        //     If cm, the number must be at least 150 and at most 193.
        //     If in, the number must be at least 59 and at most 76.
        // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        // pid (Passport ID) - a nine-digit number, including leading zeroes.
        // cid (Country ID) - ignored, missing or not.

        static ref RGX_YR: Regex = Regex::new(r"^\d{4}$").unwrap();
        static ref RGX_HGT: Regex = Regex::new(r"^(\d+)(cm|in)$").unwrap();
        static ref RGX_HCL: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        static ref RGX_ECL: Regex = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
        static ref RGX_PID: Regex = Regex::new(r"^\d{9}$").unwrap();
    }

    let byr = accu.get("byr").unwrap();
    if !RGX_YR.is_match(byr) {
        return false;
    }
    let byr = byr.parse::<i32>().unwrap();
    if byr < 1920 || byr > 2002 {
        return false;
    }

    let iyr = accu.get("iyr").unwrap();
    if !RGX_YR.is_match(iyr) {
        return false;
    }
    let iyr = iyr.parse::<i32>().unwrap();
    if iyr < 2010 || iyr > 2020 {
        return false;
    }

    let eyr = accu.get("eyr").unwrap();
    if !RGX_YR.is_match(eyr) {
        return false;
    }
    let eyr = eyr.parse::<i32>().unwrap();
    if eyr < 2020 || eyr > 2030 {
        return false;
    }

    let hgt = accu.get("hgt").unwrap();
    if let Some(cap) = RGX_HGT.captures(&hgt) {
        let value = cap.get(1).unwrap().as_str().parse::<i32>().unwrap();
        let unit = cap.get(2).unwrap().as_str();

        match unit {
            "cm" => {
                if value < 150 || value > 193 {
                    return false;
                }
            }
            "in" => {
                if value < 59 || value > 76 {
                    return false;
                }
            }
            _ => {
                return false;
            }
        }
    } else {
        return false;
    }

    let hcl = accu.get("hcl").unwrap();
    if !RGX_HCL.is_match(hcl) {
        return false;
    }

    let ecl = accu.get("ecl").unwrap();
    if !RGX_ECL.is_match(ecl) {
        return false;
    }

    let hcl = accu.get("pid").unwrap();
    if !RGX_PID.is_match(hcl) {
        return false;
    }

    true
}

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines();
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2 = 0;

    let mut accu = HashMap::new();
    for l in lines {
        if let Ok(l) = l {
            if l.len() > 0 {
                for kv in l.split(" ") {
                    let kv = kv.split(":").collect::<Vec<&str>>();
                    assert!(!accu.contains_key(kv[0]));
                    accu.insert(kv[0].to_owned(), kv[1].to_owned());
                }
            } else {
                if check_accu_p1(&accu) {
                    res_part1 += 1;
                    res_part2 += check_accu_p2(&accu) as u32;
                }
                accu.clear();
            }
        }
    }

    if accu.len() > 0 {
        if check_accu_p1(&accu) {
            res_part1 += 1;
            res_part2 += check_accu_p2(&accu) as u32;
        }
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert!(res_part1 == 245);
    assert!(res_part2 == 133);
}
