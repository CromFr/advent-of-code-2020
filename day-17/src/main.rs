#![feature(min_const_generics)]

use std::convert::TryInto;
use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use aljabar::*;


#[derive(Clone)]
struct NArray<T, const DIM: usize> {
    size: Vector<usize, DIM>,
    data: Vec<T>
}
impl<T: Copy, const DIM: usize> NArray<T, { DIM }> {
    fn new(size: &Vector<usize, DIM>, default: T) -> Self {
        let data_len = size.iter().fold(1, |acc, a| acc * a);
        Self {
            size: *size,
            data: vec![default; data_len],
        }
    }
    fn offset(&self, pos: &Vector<usize, DIM>) -> usize {
        let mut offset = 0;
        let mut mult = 1;
        for i in 0 .. DIM {
            offset += pos[i] * mult;
            mult *= self.size[i];
        }
        offset
    }
    fn reset(&mut self, value: T) {
        let len = self.data.len();
        self.data.clear();
        self.data.resize(len, value);
    }
}
impl<T: Copy, const DIM: usize> std::ops::Index<&Vector<usize, DIM>> for NArray<T, { DIM }> {
    type Output = T;
    fn index(&self, pos: &Vector<usize, DIM>) -> &Self::Output {
        &self.data[self.offset(pos)]
    }
}
impl<T: Copy, const DIM: usize> std::ops::IndexMut<&Vector<usize, DIM>> for NArray<T, { DIM }> {
    fn index_mut(&mut self, pos: &Vector<usize, DIM>) -> &mut Self::Output {
        let offset = self.offset(pos);
        &mut self.data[offset]
    }
}
impl<const DIM: usize> std::fmt::Display for NArray<bool, {DIM}> {
    /// Quick & dirty, only prints 3 dimensions
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut val = [0; DIM];
        for y in 0..self.size[1] {
                    val[1] = y;
            for z in 0..self.size[2] {
                    val[2] = z;
                for x in 0..self.size[0] {
                    val[0] = x;
                    if self[&Vector::from(val)] {
                        write!(f, "#")?;
                    } else {
                        write!(f, ".")?;
                    }
                }
                write!(f, "  ")?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}
impl<const DIM: usize> std::fmt::Display for NArray<u8, {DIM}> {
    /// Quick & dirty, only prints 3 dimensions
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut val = [0; DIM];
        for y in 0..self.size[1] {
                    val[1] = y;
            for z in 0..self.size[2] {
                    val[2] = z;
                for x in 0..self.size[0] {
                    val[0] = x;
                    write!(f, "{}", self[&Vector::from(val)])?;
                }
                write!(f, "  ")?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}



fn evolve<const DIM: usize>(init_data: &Vec<bool>, init_width: usize, init_height: usize, iterations: usize) -> NArray::<bool, DIM> {

    let mut space_size = Vector::<usize, DIM>::from([1 + 2 * iterations; DIM]);
    space_size[0] = init_width + 2 * iterations;
    space_size[1] = init_height + 2 * iterations;
    let mut space = NArray::<_, DIM>::new(&space_size, false);

    // Setup initial state
    let offset = Vector::from([iterations; DIM]);
    let mut cube_indices = vec![];
    for y in 0..init_height {
        for x in 0..init_width {
            let mut pos = offset;
            pos[0] += x;
            pos[1] += y;
            if init_data[x + y * init_width] {
                space[&pos] = true;
                cube_indices.push(space.offset(&pos));
            }
        }
    }


    // Generate indices diffs to check neighbours
    let mut neigh_diffs = vec![0i64];
    for dim in 0 .. DIM {

        let mut dim_dir = Vector::from([0; DIM]);
        dim_dir[dim] = 1;
        let dim_step = space.offset(&dim_dir);

        let mut new_neigh_diffs = vec![];
        new_neigh_diffs.extend(neigh_diffs.iter().map(|a| *a + dim_step as i64).collect::<Vec<_>>());
        new_neigh_diffs.extend(neigh_diffs.iter().map(|a| *a - dim_step as i64).collect::<Vec<_>>());
        neigh_diffs.extend(new_neigh_diffs);
    }
    neigh_diffs.remove((*neigh_diffs.iter().find(|&a| *a == 0).unwrap() as i64).try_into().unwrap());

    // Do the iterations
    let mut space_neigh = NArray::<_, DIM>::new(&space_size, 0u8);
    for _iter in 0..iterations {
        // println!("{}", space);

        for &cube_index in &cube_indices {
            for &diff in &neigh_diffs {
                if diff < 0 && diff.abs() as usize > cube_index || diff > 0 && cube_index + diff as usize >= space.data.len() {
                    continue;
                }
                space_neigh.data[(cube_index as i64 + diff) as usize] += 1;
            }
        }
        // println!("{}", space_neigh);

        cube_indices.clear();
        for (i, &count) in space_neigh.data.iter().enumerate() {
            space.data[i] = match space.data[i] {
                true => match count {
                    2 | 3 => true,
                    _ => false,
                },
                false => match count {
                    3 => true,
                    _ => false,
                },
            };
            if space.data[i] {
                cube_indices.push(i);
            }
        }

        space_neigh.reset(0);
    }

    space
}




fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, i64>::new();

    let mut init_data = vec![];
    let (mut init_width, mut init_height) = (0, 0);
    for line in lines {
        init_width = line.len();
        init_height += 1;
        init_data.append(&mut line.chars().map(|a| a == '#').collect::<Vec<_>>());
    }

    let space: NArray<_, 3> = evolve(&init_data, init_width, init_height, 6);
    aoc.set_p1(space.data.iter().fold(0, |acc, a| acc + *a as i64));

    let space: NArray<_, 4> = evolve(&init_data, init_width, init_height, 6);
    aoc.set_p2(space.data.iter().fold(0, |acc, a| acc + *a as i64));


    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 211);
            assert_eq!(aoc.p2(), 1952);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 112);
            assert_eq!(aoc.p2(), 848);
        }
        _ => panic!(),
    }
}
