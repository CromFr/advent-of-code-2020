use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

fn count_answers(group_answers: &[usize; 26], group_size: usize) -> (i32, i32) {
    let mut ret = (0, 0);
    for ans in group_answers {
        if *ans > 0 {
            ret.0 += 1;
        }
        if *ans == group_size {
            ret.1 += 1;
        }
    }

    ret
}

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines().filter_map(Result::ok);
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2 = 0;

    for line_group in lines.collect::<Vec<String>>().split(|a| a.len() == 0) {
        let mut group_answers = [0; 26];
        let mut group_size = 0;

        for l in line_group {
            group_size += 1;
            for c in l.chars() {
                assert!(c >= 'a' && c <= 'z');
                group_answers[c as usize - 'a' as usize] += 1;
            }
        }

        let cnt = count_answers(&group_answers, group_size);
        res_part1 += cnt.0;
        res_part2 += cnt.1;
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert_eq!(res_part1, 6521);
    assert_eq!(res_part2, 3305);
}
