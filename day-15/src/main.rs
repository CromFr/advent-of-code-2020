use aoc_lib::AOCResults;

fn main() {
    let mut aoc = AOCResults::<usize, usize>::new();

    let input_id = 1;
    let (input, expected_res) = match input_id {
        0 => (vec![9, 6, 0, 10, 18, 2, 1], (1238, 3745954)),
        1 => (vec![0, 3, 6], (436, 175594)),
        2 => (vec![1, 3, 2], (1, 0)),
        3 => (vec![2, 1, 3], (10, 0)),
        _ => panic!(),
    };

    // All numbers are < max range
    let mut last_numbers = vec![0 as u32; 30_000_000]; // number => prev_turn

    // Init starting values
    for (i, num) in input[0..input.len() - 1].iter().enumerate() {
        last_numbers[*num] = (i + 1) as u32;
    }
    let mut last_spoken: usize = input[input.len() - 1];

    // Loop
    for i in input.len()..30_000_000 {
        // if i % 1_000_000 == 0 {
        //     println!("iter {}K / 30K", i / 1_000_000);
        // }

        let speak = match last_numbers[last_spoken] {
            0 => 0,
            last_spoken => i as u32 - last_spoken,
        };
        last_numbers[last_spoken] = i as u32;
        last_spoken = speak as usize;

        // println!("{} => {}", i + 1, last_spoken);
        if i + 1 == 2020 {
            aoc.set_p1(last_spoken);
        }
    }
    aoc.set_p2(last_spoken);

    aoc.stats();
    assert_eq!(aoc.p1(), expected_res.0);
    assert_eq!(aoc.p2(), expected_res.1);
}
