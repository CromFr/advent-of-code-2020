use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

// #[derive(Eq, Hash)]
#[derive(Clone)]
struct Room {
    data: Vec<char>,
    width: usize,
    height: usize,
}

impl Room {
    fn get(&self, x: usize, y: usize) -> char {
        self.data[y * self.width + x]
    }
    fn get_mut(&mut self, x: usize, y: usize) -> &mut char {
        &mut self.data[y * self.width + x]
    }

    fn process(&self, cell_func: &dyn Fn(&Room, usize, usize) -> char) -> i64 {
        let mut curr_room = self.clone();

        loop {
            let prev_room = curr_room.clone();

            for y in 0..self.height {
                for x in 0..self.width {
                    if prev_room.data[y * self.width + x] == '.' {
                        continue;
                    }

                    *curr_room.get_mut(x, y) = cell_func(&prev_room, x, y);
                }
            }

            // According to the instructions, it will always end with a fixed state (no n-state loops)
            if prev_room.data == curr_room.data {
                break;
            }
        }

        curr_room.data.iter().map(|c| (*c == '#') as i64).sum()
    }
}

#[allow(dead_code)]
fn print_room(room: &Room) {
    println!("{}", "-".repeat(room.width));
    for y in 0..room.height {
        println!(
            "  {}",
            room.data[y * room.width..(y + 1) * room.width]
                .iter()
                .collect::<String>()
        );
    }
}

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, i64>::new();

    // Parse room
    let mut init_room = Room {
        data: vec![],
        width: 0,
        height: 0,
    };
    for line in lines {
        init_room.width = line.len();
        init_room.height += 1;
        init_room
            .data
            .append(&mut line.chars().collect::<Vec<char>>());
    }
    let init_room = init_room;

    // p1
    let res = init_room.process(&|room: &Room, x: usize, y: usize| {
        let mut cnt = 0;
        for iy in 0..3 {
            for ix in 0..3 {
                if ix == 1 && iy == 1 {
                    continue;
                }
                if x + ix == 0 || x + ix > room.width || y + iy == 0 || y + iy > room.height {
                    continue;
                }
                if room.get(x + ix - 1, y + iy - 1) == '#' {
                    cnt += 1
                }
            }
        }

        match room.get(x, y) {
            'L' => match cnt {
                0 => '#',
                _ => 'L',
            },
            '#' => match cnt {
                4..=8 => 'L',
                _ => '#',
            },
            _ => panic!(),
        }
    });
    aoc.set_p1(res);

    // p2
    let res = init_room.process(&|room: &Room, x: usize, y: usize| {
        let mut cnt = 0;
        for iy in 0..3 {
            for ix in 0..3 {
                if ix == 1 && iy == 1 {
                    continue;
                }

                let vector: (i64, i64) = (ix - 1, iy - 1);
                let mut pos = (x as i64 + vector.0, y as i64 + vector.1);

                while pos.0 >= 0
                    && (pos.0 as usize) < room.width
                    && pos.1 >= 0
                    && (pos.1 as usize) < room.height
                {
                    match room.get(pos.0 as usize, pos.1 as usize) {
                        '#' => {
                            cnt += 1;
                            break;
                        }
                        'L' => break,
                        _ => (),
                    }

                    pos.0 += vector.0;
                    pos.1 += vector.1;
                }
            }
        }

        match room.get(x, y) {
            'L' => match cnt {
                0 => '#',
                _ => 'L',
            },
            '#' => match cnt {
                5..=8 => 'L',
                _ => '#',
            },
            _ => panic!(),
        }
    });

    aoc.set_p2(res);

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 2222);
            assert_eq!(aoc.p2(), 2032);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 37);
            assert_eq!(aoc.p2(), 26);
        }
        _ => panic!(),
    }
}
