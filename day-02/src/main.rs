use regex::Regex;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

extern crate lazy_static;
use lazy_static::lazy_static;

fn parse_line(line: &str) -> (usize, usize, char, &str) {
    lazy_static! {
        static ref RGX: Regex = Regex::new(r"^(\d+)-(\d+) (.): (.*)$").unwrap();
    }
    let cap = RGX.captures(&line).unwrap();

    let min = cap.get(1).unwrap().as_str().parse::<usize>().unwrap();
    let max = cap.get(2).unwrap().as_str().parse::<usize>().unwrap();
    let repeat_char = cap.get(3).unwrap().as_str();
    let pass = cap.get(4).unwrap().as_str();

    assert!(repeat_char.len() == 1);
    let repeat_char = repeat_char.chars().nth(0).unwrap();

    (min, max, repeat_char, pass)
}

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines();
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2 = 0;

    for l in lines {
        if let Ok(l) = l {
            // println!("{:?}", l);

            // 1-3 a: abcde
            // 1-3 b: cdefg
            // 2-9 c: ccccccccc

            let (min, max, repeat_char, pass) = parse_line(&l);

            // part 1
            let mut cnt = 0;
            for c in pass.chars() {
                if c == repeat_char {
                    cnt += 1;
                }
            }
            if cnt >= min && cnt <= max {
                res_part1 += 1;
            }

            // part 2
            let p2cnt = (pass.chars().nth(min - 1).unwrap() == repeat_char) as usize
                + (pass.chars().nth(max - 1).unwrap() == repeat_char) as usize;
            if p2cnt == 1 {
                res_part2 += 1;
            }
        }
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert!(res_part1 == 456);
    assert!(res_part2 == 308);
}
