use aoc_lib::AOCResults;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

extern crate lazy_static;
use lazy_static::lazy_static;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    #[derive(Debug)]
    enum Instr {
        Mem { addr: u64, value: u64 },
        Mask { ones: u64, zeros: u64 },
    }
    let instructions: Vec<Instr> = lines
        .filter(|line| &line[0..1] != "#")
        .map(|line| {
            lazy_static! {
                static ref RGX_MEM: Regex = Regex::new(r"^mem\[(\d+)\] = (\d+)$").unwrap();
                static ref RGX_MASK: Regex = Regex::new(r"^mask = ([01X]+)$").unwrap();
            }

            match &line[0..3] {
                "mem" => {
                    let cap = RGX_MEM.captures(&line).unwrap();

                    Instr::Mem {
                        addr: cap.get(1).unwrap().as_str().parse::<u64>().unwrap(),
                        value: cap.get(2).unwrap().as_str().parse::<u64>().unwrap(),
                    }
                }
                "mas" => {
                    let cap = RGX_MASK.captures(&line).unwrap();
                    let value = cap.get(1).unwrap().as_str();

                    let (mut ones, mut zeros) = (0, 0);
                    for (i, c) in value.chars().enumerate() {
                        match c {
                            'X' => (),
                            '0' => zeros |= 1u64 << 35 - i,
                            '1' => ones |= 1u64 << 35 - i,
                            _ => panic!(),
                        }
                    }

                    Instr::Mask { ones, zeros }
                }
                _ => panic!(),
            }
        })
        .collect();

    let mut aoc = AOCResults::<u64, u64>::new();

    //
    // P1
    let mut mask = (0, 0);
    let mut memory: HashMap<u64, u64> = HashMap::new();
    for instr in &instructions {
        match *instr {
            Instr::Mem { addr, value } => {
                let mut v = value;
                v |= mask.0;
                v &= !mask.1;
                memory.insert(addr, v);
            }
            Instr::Mask { ones, zeros } => {
                mask = (ones, zeros);
            }
        }
    }

    aoc.set_p1(memory.iter().map(|a| a.1).sum());

    //
    // P2
    static MASK: u64 = 0xf_ff_ff_ff_ff;

    #[derive(Debug)]
    struct MaskedInstr {
        value: u64,
        fixed_addr: u64,
        floating_addr: u64,
    }
    let mut masked_instrs = vec![];

    let (mut addr_or, mut addr_floating) = (0, 0);
    for instr in &instructions {
        match instr {
            Instr::Mem { addr, value } => masked_instrs.push(MaskedInstr {
                value: *value,
                fixed_addr: *addr | addr_or,
                floating_addr: addr_floating,
            }),
            Instr::Mask { ones, zeros } => {
                addr_or = *ones;
                addr_floating = (!*ones & !*zeros) & MASK;
            }
        }
    }

    let mut sum = 0;

    // let mut bits_allset = 0u64;
    let mut bits_ones = 0u64;
    let mut bits_zeros = 0u64;

    // println!("masked_instrs={:?}", masked_instrs);
    for mi in masked_instrs.iter_mut().rev() {
        // println!(
        //     "\nSET   0 {:036b}\n      1 {:036b}\n    all {:036b}",
        //     bits_zeros & MASK,
        //     bits_ones & MASK,
        //     bits_ones & bits_zeros & MASK
        // );
        // println!(
        //     "MI  FIX {:036b}\n    FLO {:036b}",
        //     mi.fixed_addr & MASK,
        //     mi.floating_addr & MASK
        // );

        let effective_floating_bits = (mi.floating_addr & !(bits_ones | bits_zeros)) & MASK;
        let effective_ones = (mi.fixed_addr & !bits_ones) & MASK;
        let effective_zeros = (!mi.fixed_addr & !bits_zeros) & MASK;

        // println!(
        //     "EFF   0 {:036b}\n      1 {:036b}\n    flo {:036b}",
        //     effective_zeros & MASK,
        //     effective_ones & MASK,
        //     effective_floating_bits & MASK
        // );

        if effective_floating_bits > 0 || effective_ones > 0 || effective_zeros > 0 {
            let multiplier = 1 << effective_floating_bits.count_ones();

            // println!(" => +{:?}", multiplier * mi.value);
            sum += multiplier * mi.value;

            bits_ones |= effective_ones | effective_floating_bits;
            bits_zeros |= effective_zeros | effective_floating_bits;
        }
    }

    aoc.set_p2(sum);

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 13865835758282);
            // assert_eq!(aoc.p2(), 4195339838136);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 165);
            // assert_eq!(aoc.p2(), 0);
        }
        "input.ex2" => {
            // assert_eq!(aoc.p1(), 0);
            assert_eq!(aoc.p2(), 208);
        }
        "input.custom" => {}
        _ => panic!(),
    }
}
