use aoc_lib::AOCResults;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut player = usize::MAX;
    let mut decks = vec![VecDeque::new(); 2];
    for line in lines {
        if line.starts_with("Player ") {
            player = line[7..line.len() - 1].parse::<usize>().unwrap();
        } else if line.len() > 0 {
            decks[player - 1].push_back(line.parse::<usize>().unwrap())
        }
    }

    let mut aoc = AOCResults::<usize, usize>::new();

    // P1
    {
        let mut decks = decks.clone();
        while decks[0].len() != 0 && decks[1].len() != 0 {
            let p1_card = decks[0].pop_front().unwrap();
            let p2_card = decks[1].pop_front().unwrap();

            if p1_card > p2_card {
                decks[0].push_back(p1_card);
                decks[0].push_back(p2_card);
            } else {
                decks[1].push_back(p2_card);
                decks[1].push_back(p1_card);
            }
        }

        let winning_deck = if decks[0].len() > 0 {
            &decks[0]
        } else {
            &decks[1]
        };
        // println!("winning_deck={:?}", winning_deck);
        aoc.set_p1(
            winning_deck
                .into_iter()
                .rev()
                .enumerate()
                .map(|a| (a.0 + 1) * a.1)
                .sum(),
        );
    }

    // p2
    {
        fn play(decks: &mut Vec<VecDeque<usize>>) -> bool {
            let mut prev_pairings = HashSet::new();
            while decks[0].len() > 0 && decks[1].len() > 0 {
                if !prev_pairings.insert(decks.clone()) {
                    return true; //player 1 wins
                }
                let p1_card = decks[0].pop_front().unwrap();
                let p2_card = decks[1].pop_front().unwrap();

                let p1_win = if p1_card <= decks[0].len() && p2_card <= decks[1].len() {
                    let mut next_deck = decks.clone();
                    next_deck[0].resize(p1_card, 0);
                    next_deck[1].resize(p2_card, 0);
                    play(&mut next_deck)
                } else {
                    p1_card > p2_card
                };

                if p1_win {
                    decks[0].push_back(p1_card);
                    decks[0].push_back(p2_card);
                } else {
                    decks[1].push_back(p2_card);
                    decks[1].push_back(p1_card);
                }
            }

            decks[1].len() == 0
        }

        let mut decks = decks.clone();
        let p1_won = play(&mut decks);
        let winning_deck = if p1_won { &decks[0] } else { &decks[1] };
        println!("winning_deck={:?}", winning_deck);
        aoc.set_p2(
            winning_deck
                .into_iter()
                .rev()
                .enumerate()
                .map(|a| (a.0 + 1) * a.1)
                .sum(),
        );
    }

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 33561);
            assert_eq!(aoc.p2(), 34594);
        }
        "input.ex" => {
            assert_eq!(aoc.p1(), 306);
            assert_eq!(aoc.p2(), 291);
        }
        _ => panic!(),
    }
}
