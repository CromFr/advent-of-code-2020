#!/bin/bash

set -euo pipefail

EXEC=$1
shift
CNT=${1:-20}
shift

rm /tmp/bench.txt || true


for _ in $(seq 1 "$CNT"); do
	"$EXEC" "$@" | tee -a /tmp/bench.txt

	sleep 0.1
done


P1=100000
while read -r DUR; do
	P1=$(bc <<< "if($DUR < $P1) $DUR else $P1")
done < <(grep -oE '^dur_1:.*$' /tmp/bench.txt | sed -E 's/^dur_.:\s+([0-9\.]+)ms$/\1/g')


P2=100000
while read -r DUR; do
	P2=$(bc <<< "if($DUR < $P2) $DUR else $P2")
done < <(grep -oE '^dur_2:.*$' /tmp/bench.txt | sed -E 's/^dur_.:\s+([0-9\.]+)ms$/\1/g')

TOT=100000
while read -r DUR; do
	TOT=$(bc <<< "if($DUR < $TOT) $DUR else $TOT")
done < <(grep -oE '^total:.*$' /tmp/bench.txt | sed -E 's/^total:\s+([0-9\.]+)ms$/\1/g')


echo "==============================="
printf "dur_1: %7.3fms\n" "$P1"
printf "dur_2: %7.3fms\n" "$P2"
printf "total: %7.3fms\n" "$TOT"
echo "==============================="