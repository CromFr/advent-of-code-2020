use aoc_lib::AOCResults;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug, Copy, Clone)]
enum Token {
    Number(i64),
    Operator(char),
    Parens(bool),
    // Children(Vec<Token>),
}

fn lex(expression: &str) -> Vec<Token> {
    let mut tokens = vec![];

    // Lexing
    #[derive(Debug)]
    enum LexState {
        Numeric,
        Digits,
        Operator,
    }

    let mut char_acc = String::new();
    let mut state = LexState::Numeric;

    let mut it = expression.chars().chain("\0".chars());
    let mut itr = it.next();
    while let Some(c) = itr {
        if c != ' ' {
            match state {
                LexState::Numeric => match c {
                    '0'..='9' => {
                        state = LexState::Digits;
                        continue;
                    }
                    '(' => {
                        tokens.push(Token::Parens(true));
                        state = LexState::Numeric;
                    }
                    _ => panic!("Expecting a number, got character {:?}", c),
                },
                LexState::Digits => match c {
                    '0'..='9' => {
                        char_acc.push(c);
                    }
                    _ => {
                        tokens.push(Token::Number(char_acc.parse().unwrap()));
                        char_acc.clear();

                        state = LexState::Operator;
                        continue;
                    }
                },
                LexState::Operator => match c {
                    '+' | '-' | '*' | '/' => {
                        tokens.push(Token::Operator(c));
                        state = LexState::Numeric;
                    }
                    '\0' => (),
                    ')' => {
                        tokens.push(Token::Parens(false));
                        state = LexState::Operator;
                    }
                    _ => panic!("Expecting an operator, got character {:?}", c),
                },
            }
        }

        itr = it.next();
    }

    tokens
}

fn exec_expr_accu(expression: &str) -> i64 {
    let tokens = lex(expression);

    #[derive(Debug)]
    struct Context {
        value: i64,
        op: char,
        op_value: i64,
    }
    impl Context {
        fn new() -> Self {
            Self {
                value: 0,
                op: '+',
                op_value: 0,
            }
        }
        fn do_op(&mut self) {
            self.value = match self.op {
                '+' => self.value + self.op_value,
                '-' => self.value - self.op_value,
                '*' => self.value * self.op_value,
                '/' => self.value / self.op_value,
                _ => panic!("Invalid operator {:?}", self.op),
            };
            self.op = '\0';
            self.op_value = 0;
        }
    }

    let mut stack = Vec::<Context>::new();
    stack.push(Context::new());

    for token in tokens {
        let stack_i = stack.len() - 1;
        match token {
            Token::Parens(true) => {
                stack.push(Context::new());
            }
            Token::Parens(false) => {
                stack[stack_i - 1].op_value = stack[stack_i].value;
                stack[stack_i - 1].do_op();
                stack.pop();
            }
            Token::Number(num) => {
                stack[stack_i].op_value = num;
                stack[stack_i].do_op();
            }
            Token::Operator(op) => {
                stack[stack_i].op = op;
            }
        }
    }

    assert_eq!(stack.len(), 1, "Missing closing parenthesis");
    if stack[0].op != '\0' {
        stack[0].do_op();
    }
    stack[0].value
}

fn exec_expr_ast(expression: &str) -> i64 {
    fn find_matching_parens(tokens: &[Token]) -> Result<usize, ()> {
        let mut stack = 0;
        for (i, token) in tokens.iter().enumerate() {
            match token {
                Token::Parens(true) => {
                    stack += 1;
                }
                Token::Parens(false) => {
                    stack -= 1;
                    if stack == 0 {
                        return Ok(i);
                    }
                }
                _ => (),
            }
        }

        Err(())
    }

    fn calc(tokens: &[Token], _deepness: usize) -> i64 {
        // println!("{}: pass1 tokens={:?}", _deepness, tokens);
        let mut next_tokens = Vec::<Token>::new();

        // First pass, remove parenthesis
        let mut skip = 0;
        for (i, token) in tokens.iter().enumerate() {
            if i < skip {
                continue;
            }

            match token {
                Token::Parens(true) => {
                    let end = find_matching_parens(&tokens[i..]).unwrap();
                    next_tokens.push(Token::Number(calc(&tokens[i + 1..i + end], _deepness + 1)));
                    skip = i + end + 1;
                }
                _ => {
                    next_tokens.push(*token);
                }
            }
        }

        // Second pass do + operations
        let tokens = next_tokens.clone();
        // println!("{}: pass2 tokens={:?}", _deepness, tokens);
        next_tokens.clear();

        let mut hungry = false;
        for token in &tokens {
            match token {
                Token::Operator('+') => {
                    hungry = true;
                }
                Token::Number(num) => {
                    if hungry {
                        let next_tokens_len = next_tokens.len();
                        if let Token::Number(prev_num) = next_tokens[next_tokens_len - 1] {
                            let res = prev_num + num;
                            next_tokens[next_tokens_len - 1] = Token::Number(res);
                            hungry = false;
                        } else {
                            panic!()
                        }
                    } else {
                        next_tokens.push(*token);
                    }
                }
                _ => {
                    next_tokens.push(*token);
                }
            }
        }

        // println!("{}: pass3 tokens={:?}", _deepness, tokens);
        // Third pass, do remaining ops
        let mut value = 0;
        let mut last_op = '+';
        for token in &next_tokens {
            match token {
                Token::Number(num) => {
                    value = match last_op {
                        '+' => value + num,
                        '-' => value - num,
                        '*' => value * num,
                        '/' => value / num,
                        _ => panic!("Invalid operator {:?}", last_op),
                    };
                }
                Token::Operator(op) => {
                    last_op = *op;
                }
                _ => panic!(),
            }
        }

        return value;
    }

    let tokens = lex(expression);
    calc(&tokens, 0)
}

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    // Tests
    assert_eq!(exec_expr_accu("2 * 3 + 4 * 5"), 50);
    assert_eq!(exec_expr_accu("2 * 3 + (4 * 5)"), 26);
    assert_eq!(exec_expr_accu("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 437);
    assert_eq!(
        exec_expr_accu("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
        12240
    );
    assert_eq!(
        exec_expr_accu("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
        13632
    );

    assert_eq!(exec_expr_ast("1 + (2 * 3) + (4 * (5 + 6))"), 51);
    assert_eq!(exec_expr_ast("2 * 3 + (4 * 5)"), 46);
    assert_eq!(exec_expr_ast("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445);
    assert_eq!(
        exec_expr_ast("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
        669060
    );
    assert_eq!(
        exec_expr_ast("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
        23340
    );

    // AOC result
    let mut aoc = AOCResults::<i64, i64>::new();

    let exprs: Vec<_> = lines.collect();
    aoc.set_p1(exprs.iter().map(|a| exec_expr_accu(a)).sum());
    aoc.set_p2(exprs.iter().map(|a| exec_expr_ast(a)).sum());

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 67800526776934);
            assert_eq!(aoc.p2(), 340789638435483);
        }
        _ => panic!(),
    }
}
