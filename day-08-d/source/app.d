import std;
import std.datetime.stopwatch: StopWatch;

alias Instruction = Tuple!(string, int);

struct Context {
    size_t pc = 0;
    int acc = 0;
    bool[] executed;

    this(in size_t program_size) {
    	executed.length = program_size;
    	executed[] = false;
    }

    void execute(in Instruction instruction) {
        executed[pc] = true;

        switch(instruction[0]) {
        	case "acc": acc += instruction[1]; break;
        	case "jmp": pc += instruction[1]; return;
        	case "nop": break;
        	default: assert(0);
        }

        pc++;
    }

    auto execute_program(in Instruction[] program) {

    	while(true) {
            assert(pc <= program.length);
            if (pc == program.length) {
                return tuple("end", acc);
            }

            if (executed[pc]) {
                // Loop detected
                return tuple("loop", acc);
            }
            execute(program[pc]);
    	}
    }
}


auto part1(in Instruction[] program){
	auto context = Context(program.length);
	auto res = context.execute_program(program);
	assert(res[0] == "loop");
	return res[1];
}

auto part2(in Instruction[] program){

	foreach(i ; 0 .. program.length){
		if(program[i][0] == "acc")
			continue;

		auto new_program = program.dup();
		new_program[i][0] = new_program[i][0] == "jmp" ? "nop" : "jmp";

		auto context = Context(program.length);
		auto res = context.execute_program(new_program);
		if(res[0] == "end")
			return res[1];
	}

	return 0;
}

void main()
{
	StopWatch sw;
	sw.start();

	enum program = import("input")
		.splitLines
		.map!((l){
			auto spl = l.split(" ");
			return Instruction(spl[0], spl[1].to!int);
		})
		.array;

	enum res_part1 = part1(program);
	enum res_part2 = part2(program);

	sw.stop();

	writefln("res_part1=%d", res_part1);
	writefln("res_part2=%d", res_part2);
	writefln("Time=%fµs", sw.peek.total!"nsecs" / 1000.0);

}
