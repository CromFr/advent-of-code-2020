use aoc_lib::AOCResults;
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let file_name = "input";
    let file = File::open(file_name).expect("Input file not found");
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, i64>::new();

    let mut adapters = vec![];
    for line in lines {
        adapters.push(line.parse::<i64>().expect("Bad input number"));
    }

    adapters.push(0);
    adapters.sort();
    adapters.push(adapters[adapters.len() - 1] + 3);

    let mut combinations = VecDeque::new();
    combinations.push_back(1);
    combinations.push_back(0);
    combinations.push_back(0);

    let mut last_rating = 0;
    let (mut onediff, mut threediff) = (0, 0);
    for rating in adapters {
        let diff = rating - last_rating;
        if diff == 1 {
            onediff += 1;
        } else if diff == 3 {
            threediff += 1;
        }

        // Remove inaccessible combinations
        for _ in 0..diff {
            combinations.pop_front();
        }

        let total_combin = combinations.iter().sum();

        // Zero out skipped values
        for _ in 1..diff {
            combinations.push_back(0);
        }
        combinations.push_back(total_combin);

        assert!(combinations.len() == 4);

        last_rating = rating;
    }
    aoc.set_p1(onediff * threediff);
    aoc.set_p2(combinations[0]);

    aoc.stats();
    match file_name {
        "input" => {
            assert_eq!(aoc.p1(), 2277);
            assert_eq!(aoc.p2(), 37024595836928);
        }
        "input.ex1" => {
            assert_eq!(aoc.p1(), 35);
            assert_eq!(aoc.p2(), 8);
        }
        "input.ex2" => {
            assert_eq!(aoc.p1(), 220);
            assert_eq!(aoc.p2(), 19208);
        }
        _ => panic!(),
    }
}
