use aoc_lib::AOCResults;
use core::cmp::{max, min};
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn sorted_insert(vec: &mut Vec<i64>, value: i64) {
    match vec.binary_search(&value) {
        Err(pos) => vec.insert(pos, value),
        _ => panic!("Value {} already in {:?}", value, vec),
    }
}
fn sorted_remove(vec: &mut Vec<i64>, value: i64) {
    match vec.binary_search(&value) {
        Ok(pos) => {
            vec.remove(pos);
        }
        _ => panic!(),
    }
}

fn find_sum(sorted_values: &Vec<i64>, target: i64) -> Option<(i64, i64)> {
    let mut min = 0;
    let mut max = sorted_values.len() - 1;
    while min < max {
        let res = sorted_values[min] + sorted_values[max];
        if res == target {
            return Some((sorted_values[min], sorted_values[max]));
        } else if res < target {
            min += 1;
        } else {
            max -= 1;
        }
    }

    return None;
}

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines().filter_map(Result::ok);

    let mut aoc = AOCResults::<i64, i64>::new();

    let mut values = vec![];
    let mut last_values = VecDeque::new();
    let mut last_sorted_values = vec![];

    for line in lines {
        let value = line.parse::<i64>().unwrap();
        values.push(value);

        if last_values.len() >= 25 {
            assert!(last_values.len() == 25);

            match find_sum(&last_sorted_values, value) {
                Some(_) => (),
                None => {
                    aoc.set_p1(value);

                    'contig_start: for start in 0..values.len() {
                        let start_value = values[start];
                        if start_value > aoc.p1() {
                            continue;
                        }

                        let mut total = 0;
                        let (mut min_val, mut max_val) = (start_value, start_value);
                        for i in start..values.len() {
                            min_val = min(values[i], min_val);
                            max_val = max(values[i], max_val);

                            total += values[i];
                            if total == aoc.p1() {
                                aoc.set_p2(min_val + max_val);
                                break 'contig_start;
                            } else if total > aoc.p1() {
                                break;
                            }
                        }
                    }
                }
            }

            let to_remove = last_values.pop_front().unwrap();
            sorted_remove(&mut last_sorted_values, to_remove);
        }
        last_values.push_back(value);
        sorted_insert(&mut last_sorted_values, value);
    }

    aoc.stats();
    assert_eq!(aoc.p1(), 41682220);
    assert_eq!(aoc.p2(), 5388976);
}
