use regex::Regex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::SystemTime;

extern crate lazy_static;
use lazy_static::lazy_static;

fn main() {
    let file = File::open("input").unwrap();
    let lines = BufReader::new(file).lines().filter_map(Result::ok);
    let start = SystemTime::now();

    let mut res_part1 = 0;
    let mut res_part2 = 0;

    lazy_static! {
        static ref RGX_LINE: Regex = Regex::new(r"^(.*?)\s+bags contain\s+(.*?)\.$").unwrap();
    }

    type ChildrenList = HashMap<String, usize>;
    // Color => list and number of bags it can contain
    let mut bag_children: HashMap<String, ChildrenList> = HashMap::new();

    for line in lines {
        let m = RGX_LINE.captures(&line).expect("Line did not match regex");

        let name = m.get(1).unwrap().as_str().to_owned();
        let mut children: ChildrenList = HashMap::new();

        for kv in m
            .get(2)
            .unwrap()
            .as_str()
            .split(", ")
            .filter(|a| *a != "no other bags")
            .map(|a| {
                let sep = a.find(' ').unwrap();
                let end = a.find(" bag").unwrap();
                (
                    a[sep + 1..end].to_owned(),
                    a[..sep].parse::<usize>().unwrap(),
                )
            })
        {
            children.insert(kv.0, kv.1);
        }

        bag_children.insert(name, children);
    }

    // ===================> Part 1

    // Reverse list
    // Color => list of bags that can contain it
    let mut bag_parents: HashMap<String, HashSet<String>> = HashMap::new();
    for (parent_name, children) in &bag_children {
        for (child_name, _child_count) in children {
            if !bag_parents.contains_key(child_name) {
                bag_parents.insert(child_name.clone(), HashSet::new());
            }
            bag_parents
                .get_mut(child_name)
                .unwrap()
                .insert(parent_name.clone());
        }
    }

    let mut to_inspect = vec!["shiny gold".to_owned()];
    let mut res_part1_containers = HashSet::new();

    while to_inspect.len() > 0 {
        let mut next_to_inspect = vec![];

        for bag_name in &to_inspect {
            match bag_parents.get(bag_name) {
                Some(l) => {
                    for k in l {
                        next_to_inspect.push(k.clone());
                        res_part1_containers.insert(k.clone());
                    }
                }
                None => (),
            }
        }
        to_inspect = next_to_inspect;
    }
    res_part1 = res_part1_containers.len();

    // ===================> Part 2

    let mut to_inspect = vec![("shiny gold".to_owned(), 1)];
    while to_inspect.len() > 0 {
        let mut next_to_inspect = vec![];

        for (bag_name, bag_count) in &to_inspect {
            for (child_name, child_count) in bag_children.get(bag_name).unwrap() {
                next_to_inspect.push((child_name.to_owned(), *child_count * bag_count));
                res_part2 += child_count * bag_count;
            }
        }

        to_inspect = next_to_inspect;
    }

    println!("res_part1: {}", res_part1);
    println!("res_part2: {}", res_part2);
    println!(
        "Time: {}ms",
        start.elapsed().unwrap().as_micros() as f32 / 1000.0
    );
    assert_eq!(res_part1, 124);
    assert_eq!(res_part2, 34862);
}
