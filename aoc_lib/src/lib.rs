use std::time::Duration;
use std::time::SystemTime;

pub struct AOCResults<P1, P2> {
    p1: Option<P1>,
    p2: Option<P2>,
    ts_start: SystemTime,
    dur_p1: Option<Duration>,
    dur_p2: Option<Duration>,
}

impl<P1: std::fmt::Display + Copy, P2: std::fmt::Display + Copy> AOCResults<P1, P2> {
    pub fn new() -> AOCResults<P1, P2> {
        AOCResults::<P1, P2> {
            p1: None,
            p2: None,
            ts_start: SystemTime::now(),
            dur_p1: None,
            dur_p2: None,
        }
    }

    pub fn p1(&self) -> P1 {
        self.p1.expect("p1 not submitted")
    }
    pub fn p2(&self) -> P2 {
        self.p2.expect("p2 not submitted")
    }

    pub fn set_p1(&mut self, res: P1) {
        self.p1 = Some(res);
        self.dur_p1 = Some(self.ts_start.elapsed().unwrap());
    }
    pub fn set_p2(&mut self, res: P2) {
        self.p2 = Some(res);
        self.dur_p2 = Some(
            self.ts_start.elapsed().unwrap() - self.dur_p1.expect("Part 1 results not submitted"),
        );
    }

    pub fn stats(&self) {
        println!(
            "=== Res ===\nres_1: {}\nres_2: {}",
            match &self.p1 {
                Some(r) => r.to_string(),
                None => "UNSUBMITTED".to_string(),
            },
            match &self.p2 {
                Some(r) => r.to_string(),
                None => "UNSUBMITTED".to_string(),
            },
        );

        let dur_p1 = self.dur_p1.unwrap_or(Duration::new(0, 0));
        let dur_p2 = self.dur_p2.unwrap_or(Duration::new(0, 0));
        println!(
            "=== Bench ===\ndur_1: {:7.3}ms\ndur_2: {:7.3}ms\ntotal: {:7.3}ms",
            dur_p1.as_micros() as f32 / 1000.0,
            dur_p2.as_micros() as f32 / 1000.0,
            (dur_p1 + dur_p2).as_micros() as f32 / 1000.0,
        );
    }
}
